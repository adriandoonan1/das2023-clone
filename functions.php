<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );    

function enqueue_parent_styles() {

	wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri().'/css/theme.css' );
	wp_enqueue_script( 'das2023-bootstrap', get_stylesheet_directory_uri() . '/js/theme-bootstrap4.min.js', array(), 1, true );
	wp_enqueue_script( 'das2023-scripts', get_stylesheet_directory_uri() . '/js/theme.js', array(), 2, true );
}

/**
 * Enable shortcodes for menu navigation.
 */
if ( ! has_filter( 'wp_nav_menu', 'do_shortcode' ) ) {
    add_filter( 'wp_nav_menu', 'shortcode_unautop' );
    add_filter( 'wp_nav_menu', 'do_shortcode', 11 );
}

function pre_r($var) {

	if(!isset( $_GET['debug'] )) return false;
	
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}
 
show_admin_bar(false); 

include "inc/das_events.php";
include "inc/das_carousel.php";

das_events::init();
     
?>