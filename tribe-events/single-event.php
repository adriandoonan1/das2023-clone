<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single">

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); 

	das_events::print_ticket_button( $event_id );

	?>

	<div class="tribe-events-schedule tribe-clearfix">
		<?php echo tribe_events_event_schedule_details( $event_id, '<h2>', '</h2>' ); ?>
		<?php if ( tribe_get_cost() ) : ?>
			<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
	</div>

	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- Event featured image, but exclude link -->
			<div class="row">
				<div class="col-md-4">
					<?php das_events::print_event_image( $event_id ); ?>
				</div><!-- .col-md-6 -->
				<div class="col-md-8">
					<div class="tribe-events-single-event-description tribe-events-content">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12">
					<?php tribe_get_template_part( 'modules/meta' ); 

					das_events::print_participants( $event_id );
					?>
				</div><!-- .col-12 -->
			</div>
			
		</div> <!-- #post-x -->
	<?php endwhile; ?>

</div><!-- #tribe-events-content -->