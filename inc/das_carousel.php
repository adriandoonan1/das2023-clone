<?php
if ( ! class_exists( 'das_carousel' ) ) {

	class das_carousel {

		private $carousel;

		const SIZES = ['small','medium','full'];
		
		public function __construct() {

			$this->carousel = get_field('carousel');

			if($this->carousel) {

				shuffle($this->carousel);
				$carousel = $this->get_carousel();

			} else {

				$carousel = $this->get_featured_image();

			}

			

			echo $carousel;

		}

		private function get_carousel() {

			$ht = '
			<div id="das-carousel" class="das-carousel carousel slide" data-ride="carousel">
				<h1 class="carousel-title display-2">' . get_the_title() . '</h1>
				<div class="carousel-inner">';

			foreach( $this->carousel as $i => $carousel) {

				$active = ($i==0) ? " active" : "";

				$ht .= '<div class="carousel-item' . $active . '">';
				$ht .= $this->get_featured_image($carousel);
				$ht .= '</div><!-- .carousel-item -->';
			}

			$ht .= '
					<div class="carousel-caption d-none d-md-block">
						<p>Photographer <a href="https://markanthonypratt.com" target="new">Mark Anthony Pratt</a></p>
					</div>
				</div><!-- .carousel-inner -->
			</div><!-- .carousel -->';

			return $ht;
 
		}

		private function get_featured_image($img_obj = false) {

			$ht = '
			<div class="main-image-container">';
			
			$ht .= !$img_obj ? '<h1 class="main-image-heading display-2">' . get_the_title() . '</h1>' : '';

			foreach (static::SIZES as $size) {

				$image = $img_obj ? wp_get_attachment_image($img_obj["ID"], $size) : get_the_post_thumbnail( get_the_id(), $size );

				$ht .= '<div class="featured-image i-' . $size . '">' . $image . '</div>';
				
			}

			$ht .= '</div>';

			return $ht;
			
		}

	}

}

?>