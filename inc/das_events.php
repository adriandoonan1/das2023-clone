<?php
if ( ! class_exists( 'das_events' ) ) {

	class das_events {

		CONST human_day_format = "l F j";
		CONST human_event_list_format = "g:ia";
		CONST default_ticket_link = "https://comedycafeberlin.com";
		CONST home_page_id = 80;

		public static $venue;

		public static $cat;

		public static $no_filter;

		public static $das_signup_link;
		
		static function init() {

			self::$das_signup_link = get_field('signup_form_url', self::home_page_id);

			add_shortcode('das_list_events', 'das_events::das_list_events'); 
			add_shortcode('das_signup_link', 'das_events::das_signup_link'); 
			add_shortcode('das_signup_link_html', 'das_events::das_signup_link_html'); 

		}

		static function das_signup_link_html($text) {

			$text = $text ? $text : "Apply Now";

			return ('<a href="' . self::$das_signup_link . '">' . $text . '</a>');

		}

		static function das_signup_link() {

			return self::$das_signup_link;

		}

		static function group_tribe_events_by_day() {

			if (isset($_GET['venue'])) self::$venue = $_GET['venue'];
			if (isset($_GET['cat'])) self::$cat = $_GET['cat'];

			if(!isset(self::$venue) && !isset(self::$cat)) self::$no_filter = true;

			$args = ([
				'eventDisplay' => 'list',
				'posts_per_page' => -1
			]); 

			if(self::$venue) $args["venue"] = self::$venue;
			if(self::$cat) $args["event_category"] = self::$cat;

			$events = tribe_get_events($args); 

			//add extra info we need
			foreach ($events as $i => $event) {
				$events[$i]->day = substr($events[$i]->event_date, 0, 10);
				$events[$i]->venue = tribe_get_venue( $events[$i]->ID );
				$events[$i]->categories = tribe_get_event_cat_ids( $events[$i]->ID );
			}

			$arr = array();

			foreach ($events as $key => $item) {
			   $arr[$item->day][$key] = $item;
			}

			ksort($arr, SORT_NUMERIC);

			return $arr;
		}

		static function format_day($date) {

			return date(self::human_day_format, strtotime($date)); 

		}

		static function format_event_list_date($date) {

			return date(self::human_event_list_format, strtotime($date));

		}

		static function get_all_participants($id) {

			$participants = self::get_participants($id);

			if(empty($participants)) return false;
			$all_participants = [];

			foreach ($participants as $p) {

				if(isset($p["individual"]->ID)) {

					$p["individual"]->thumbnail = get_the_post_thumbnail_url( $p["individual"]->ID, 'thumbnail' );
					array_push($all_participants, $p["individual"]);

				}

				if(isset($p["team"]->ID)) {

					$members = get_field("members", $p["team"]->ID);
					
					foreach ($members as $m) {
						$m["member"]->role = $m["role"];
						$m["member"]->thumbnail = get_the_post_thumbnail_url( $m["member"]->ID, 'thumbnail' );
						array_push($all_participants, $m["member"]);
					}

				}

			}

			return $all_participants;
		}

		static function print_participants($id) {

			$all_participants = self::get_all_participants($id);

			//pre_r($all_participants);

			if(empty($all_participants)) return false;

			$ht = '<h3 class="das-participants-list-header">Featuring:</h3>
			<ul class="das-participants-list list-inline">';

			foreach ($all_participants as $p) {
				
				$ht .= '
				<li class="das-participant list-inline-item">
					<a href="' . $p->guid . '">
						<img src="' . $p->thumbnail . '">	
						<p class="das-participant-title">' . $p->post_title . '</p>
					</a>
				</li>';
			}

			$ht .= '</ul>';

			echo $ht;


		}

		static function get_participants($id) {

			return get_field("people", $id);

		}

		static function list_event_participants($id) {

			$participants = self::get_participants($id);

			$arr = [];

			foreach ($participants as $p) {
				if($p["team"]) array_push($arr, $p["team"]->post_title);
				if($p["individual"]) array_push($arr, $p["individual"]->post_title);
			}

			return implode(", ", $arr);

		}

		static function get_event_classes($obj) {

			$venue = "venue-" . sanitize_title($obj->venue);
			$cats = "cat-" . implode(" cat-", $obj->categories);

			return $venue . " " . $cats; 

		}

		static function print_ticket_button($id) {

			echo self::get_ticket_button($id);

		}

		static function get_ticket_button($id) {

			$url = get_post_meta( $id, "_EventURL" );

			$url = $url[0] ? $url[0] : self::default_ticket_link;

			return '<a href="' . $url . '" class="btn btn-primary event-ticket">Get Tickets</a>';

		}

		static function get_event_filters() {

			$venues = get_posts( [
			    'numberposts'      => -1,
			    'post_type'        => 'tribe_venue'
			]);

			$terms = get_terms( array(
			    'taxonomy' => 'tribe_events_cat',
			    'hide_empty' => true,
			) );

			$output = '
			<div class="btn-toolbar filter-shows" role="toolbar" aria-label="Filter shows">
			<div data-toggle="buttons" class="toggle-buttons">
			<form class="event-filter-form row" method="get">
			<div class="col">
			<div class="btn-group" role="group" aria-label="Venues">
			<div class="btn-group-inner">
			<label class="btn-group-label">Venue</label>';
			foreach ($venues as $i => $venue) {

				if(self::$no_filter || !isset(self::$venue)) {

					$checked = " checked";
					$checked_class = " active";

				} else {

					$checked = in_array($venue->ID, self::$venue) ? " checked" : "";
					$checked_class = in_array($venue->ID, self::$venue) ? " active" : "";

				} 

				$output .= '<label class="btn ' . $checked_class . '" for="btn-check"><i class="icon fa"></i>
							' . $venue->post_title . '<input type="checkbox" name="venue[]" value="' .  $venue->ID . '" autocomplete="off" ' . $checked . '></label>';
			}
			$output .= '
			</div><!-- .btn-group-inner -->
			</div><!-- .btn-group -->
			</div><!-- .col -->
			<div class="col">
			<div class="btn-group" role="group" aria-label="Type">
			<div class="btn-group-inner">
			<label class="btn-group-label">Event type</label>';

			foreach ($terms as $i => $term) {

				if(self::$no_filter || !isset(self::$cat)) {

					$checked = " checked";
					$checked_class = " active";

				} else {

					$checked = in_array($term->term_taxonomy_id, self::$cat) ? " checked" : "";
					$checked_class = in_array($term->term_taxonomy_id, self::$cat) ? " active" : "";

				}


				$output .= '<label class="btn' . $checked_class . '" for="btn-check"><i class="icon fa"></i><input type="checkbox" name="cat[]" value="' .  $term->term_taxonomy_id . '" autocomplete="off" ' . $checked . '>
							' . $term->name . '</label>';
			}
			$output .= '

			</div><!-- .btn-group-inner -->
			</div><!-- .btn-group -->
			</div><!-- .col -->
			</form>
			</div><!-- /toggle -->
			</div><!-- .btn-toolbar -->';

			return $output;
		}

		static function print_event_image($post_id) {

			echo self::get_event_image($post_id);

		}

		static function get_excerpt($id, $length = 20) {

			$content = get_post_field('post_content', $id);
			$content = strip_shortcodes($content);
			$content = strip_tags($content);

			$content_length = strlen($content);

			if($content_length == 0) {

				$people = self::get_participants($id);

				foreach($people as $i => $p) {

					$key = (isset($p["team"]->post_content)) ? "team" : "individual";

					$content .= $p[$key]->post_content;

					$content_length = strlen($content);

					if ($i !== array_key_last($people)) {

						$content .= "<br><br>";

					}
				}

			}

			$excerpt = wp_trim_words( $content, $length,'' );

			if(strlen($excerpt) < $content_length) $excerpt .= '...';

			$excerpt .= ' <a href="' . get_the_permalink($id) . '">More Details</a>';

			return $excerpt;

		}

		static function get_event_image($post_id) {

			$people = self::get_participants($post_id);

			if( get_the_post_thumbnail( $post_id, 'medium' ) ) {

				//First try and get the thumbnail for the event
				$img = get_the_post_thumbnail( $post_id, 'medium' );

			} elseif ( $people ) {

				//If that doesn't work try and get the team pi
				$person = $people[0];

				if(isset($person["team"]->ID)) {

					$img = get_the_post_thumbnail( $person["team"]->ID, 'medium' );

					if(!$img) {

						$all = self::get_all_participants( $post_id );

						foreach ($all as $p) {
							
							$img = get_the_post_thumbnail( $p->ID, 'medium' );

							if($img) break;
						}
					}

				}

				if(isset($person["individual"]->ID)) $img = get_the_post_thumbnail( $person["individual"]->ID, 'medium' );

			}

			return $img;

		}

		static function das_list_events($args) { 

			self::$cat = isset($args["cat"]) ? [$args["cat"]] : self::$cat;
			self::$venue = isset($args["venue"]) ? [$args["venue"]] : self::$venue;

			$events = self::group_tribe_events_by_day();

			$output = '<div class="das-list-events">';

			$output .= self::get_event_filters();

			foreach ($events as $key => $day) {

				$output .= '<div class="das-event-day">

								<h2 class="das-event-day-header h4 bg-secondary p-2 text-primary">' . self::format_day($key) . '</h2>
								<ul class="das-events-day">
								';

							foreach ($day as $d) {

								$cost = tribe_get_cost($d->ID) ? '<p class="cost">Price: &euro;' . tribe_get_cost($d->ID) . '</p>' : '';

								$output .= '<li class="das-event ' . self::get_event_classes($d) . '">

												<div class="row">

													<div class="col-sm-3">' . self::get_event_image($d->ID) . '</div>

													<div class="col-sm-7">

														<h3 class="event-heading h5 font-weight-bold"><a href="' . $d->guid . '">

														
															<span class="date das-event-date">' . self::format_event_list_date($d->event_date) . '</span>

															<span class="das-event-header">' . $d->post_title . '</span>
															/
															<span class="das-event-participants">' . self::list_event_participants($d->ID) . '</span>

														</a></h3>

														' . $cost . '

														<p class="event-blurb">' . self::get_excerpt($d->ID) . '</p> 

														<div class="das-event-buttons">' . self::get_ticket_button($d->ID) . '</div>

													</div>

												</div>

											</li><!-- .das-event -->';

							}

				$output .= '</ul></div><!-- .das-event-day-->';
			}

			if(count($events) == 0) {
				$output .= '<div class="p-3 mb-2 bg-danger text-light">No events found, try <a href="?p=80" class="text-dark">Resetting the filters</a></div>';
			}

			$output .= '</div><!-- .das-list-events -->';

			return $output;

		} 
	}

}

?>